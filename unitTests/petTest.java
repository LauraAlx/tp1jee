/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.springframework.samples.petclinic.model;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author o-User-DawinAlt
 */
public class petTest {
    
    public petTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void testPetHasVisits() {
        Pet pet = new Pet();
        Visit visit = new Visit();
        assertThat(pet.getVisitsInternal().isEmpty(), is(true));
        pet.addVisit(visit);
        assertThat(pet.getVisitsInternal().isEmpty(), is(false));
        assertEquals(true , pet.getVisits().contains(visit));
    }
    
    public void testPetHasOperations() {
        Pet pet = new Pet();
        Operation operation = new Operation();
        assertThat(pet.getVisitsInternal().isEmpty(), is(true));
        pet.addOperation(operation);
        assertThat(pet.getVisitsInternal().isEmpty(), is(false));
        assertEquals(true , pet.getOperations().contains(operation));
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
