/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.springframework.samples.petclinic.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author o-User-DawinAlt
 */
public class vetTest {
    
    public vetTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    public void testVetHasMemo() {
        Vet vet = new Vet();
        Memo memo = new Memo();
        assertThat(vet.getMemos().isEmpty(), is(true));
        vet.addMemo(memo);
        assertThat(vet.getMemos().isEmpty(), is(false));
        assertEquals(true , vet.getMemos().contains(memo));
    }
    
    public void testVetHasOperations() {
        Vet vet = new Vet();
        Operation operation = new Operation();
        assertThat(vet.getOperations().isEmpty(), is(true));
        vet.addOperation(operation);
        assertThat(vet.getOperations().isEmpty(), is(false));
        assertEquals(true , vet.getOperations().contains(operation));
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
